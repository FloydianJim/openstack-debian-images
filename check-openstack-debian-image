#!/bin/sh

set -e
#set -x

usage() {
	echo "Usage: $0 --release|-r <jessie|wheezy|stretch|buster|bullseye|booksworm|sid|unstable> [options]
Options are:
 --deb-mirror|-d <debian-mirror> (default: http://deb.debian.org/debian)
 --security-mirror|-s <security-mirror-url> (default: http://security.debian.org/debian-security)
Example: $0 --release buster --deb-mirror http://mirror.infomaniak.com/debian --security-mirror http://mirror.infomaniak.com/debian-security
"
	exit 1
}

for i in $@ ; do
	case "${1}" in
	"--deb-mirror"|"-d")
		if [ -z "${2}" ] ; then
			echo "No parameter for --deb-mirror / -d"
			usage
		fi
		DEB_MIRROR=${2}
		shift
		shift
	;;
	"--security-mirror"|"-s")
		if [ -z "${2}" ] ; then
			echo "No parameter for --security-mirror / -s"
			usage
		fi
		SECURITY_MIRROR=${2}
		shift
		shift
	;;
	"--release"|"-r")
		if [ -z "${2}" ] ; then
			echo "No parameter for --release / -r"
			usage
		fi
		RELEASE=${2}
		if [ "${RELEASE}" = "unstable" ] ; then
			RELEASE=sid
		fi
		shift
		shift
	;;
	*)
	;;
	esac
done

if [ -z "${DEB_MIRROR}" ] ; then
	DEB_MIRROR=http://deb.debian.org/debian
fi

if [ -z "${SECURITY_MIRROR}" ] ; then
	SECURITY_MIRROR=http://security.debian.org/debian-security
fi

if [ -z "${RELEASE}" ] ; then
	echo "--release|-r parameter missing: cannot tell what release..."
	usage
fi

# Fetch and validate InRelease
if [ "${RELEASE}" = "stretch" ] ; then
	rm -f Release Release.gpg
	wget -q ${DEB_MIRROR}/dists/${RELEASE}/Release -O Release
	wget -q ${DEB_MIRROR}/dists/${RELEASE}/Release.gpg -O Release.gpg
	gpg --no-default-keyring --keyring /usr/share/keyrings/debian-archive-keyring.gpg --verify Release.gpg Release 2>&1 | grep "Good signature from"
else
	rm -f InRelease Release Release.gpg
	wget ${DEB_MIRROR}/dists/${RELEASE}/InRelease -O InRelease
	gpg --no-default-keyring --keyring /usr/share/keyrings/debian-archive-keyring.gpg --verify InRelease 2>&1 | grep "Good signature from"
	cp InRelease Release.gpg
	gpg --no-default-keyring --keyring /usr/share/keyrings/debian-archive-keyring.gpg Release.gpg || true
	rm -f Release.gpg
fi
SOURCES_GZ_SHA256=$(cat Release | grep main/source/Sources.gz | tail -n 1 | awk '{print $1}')

# Download Sources.gz and validate it
rm -f Sources.gz Sources
wget ${DEB_MIRROR}/dists/${RELEASE}/main/source/Sources.gz -O Sources.gz
CHECK=$(sha256sum Sources.gz  | awk '{print $1}')
if [ "${SOURCES_GZ_SHA256}" != "${CHECK}" ] ; then
	echo "SHA256 sums are not equal: authentification error, exiting..."
	exit 1
else
	echo "Authentic Sources.gz"
fi
gzip -d Sources.gz

# Fetch the InRelease from security mirror
if [ "${RELEASE}" = "wheezy" ] || [ "${RELEASE}" = "jessie" ] || [ "${RELEASE}" = "stretch" ] || [ "${RELEASE}" = "buster" ] ; then
	SECURITY="/updates"
else
	SECURITY="-security"
fi
if [ "${RELEASE}" != "sid" ] ; then
	rm -f SecurityInRelease SecurityRelease SecurityRelease.gpg
	wget ${SECURITY_MIRROR}/dists/${RELEASE}${SECURITY}/InRelease -O SecurityInRelease
	gpg --no-default-keyring --keyring /usr/share/keyrings/debian-archive-keyring.gpg --verify SecurityInRelease 2>&1 | grep "Good signature from"
	cp SecurityInRelease SecurityRelease.gpg
	gpg --no-default-keyring --keyring /usr/share/keyrings/debian-archive-${RELEASE}-security-automatic.gpg SecurityRelease.gpg || true
	rm -f SecurityRelease.gpg
	SECURITY_SOURCES_GZ_SHA256=$(cat SecurityRelease | grep main/source/Sources.gz | tail -n 1 | awk '{print $1}')

	# Download Sources.gz and validate it
	rm -f SecuritySources.gz SecuritySources
	wget ${SECURITY_MIRROR}/dists/${RELEASE}${SECURITY}/main/source/Sources.gz -O SecuritySources.gz
	SECURITY_CHECK=$(sha256sum SecuritySources.gz  | awk '{print $1}')
	if [ "${SECURITY_SOURCES_GZ_SHA256}" != "${SECURITY_CHECK}" ] ; then
		echo "SHA256 sums are not equal: authentification error, exiting..."
		exit 1
	else
		echo "Authentic SecuritySources.gz"
	fi
	gzip -d SecuritySources.gz
fi

echo "=========> Starting to check if ${RELEASE} needs update <========="

PKG_LIST_REL=$(mktemp -t check-openstack-debian-image.release-sources-file.XXXXXX)
cat Sources         | grep -E '^Package: |^Version: ' | sed -e 's/^Package: //' | sed ':a;N;$!ba;s/\nVersion: / /g' >${PKG_LIST_REL}
if [ "${RELEASE}" != "sid" ] ; then
	PKG_LIST_SEC=$(mktemp -t check-openstack-debian-image.security-sources-file.XXXXXX)
	cat SecuritySources | grep -E '^Package: |^Version: ' | sed -e 's/^Package: //' | sed ':a;N;$!ba;s/\nVersion: / /g' >${PKG_LIST_SEC}
fi

LATEST=$(cat latest)
for FULL_PKG in $(cat $(cat latest)-packages.list | tr ' ' ',') ; do
	SRC_PKG=$(echo ${FULL_PKG} | cut -d, -f1)
	PKG_VER=$(echo ${FULL_PKG} | cut -d, -f2)
	echo -n "---> Checking: $SRC_PKG "
	echo -n "(In image: ${PKG_VER}"
	VERSION_IN_REL=$(cat ${PKG_LIST_REL} | grep '^'${SRC_PKG}' ' | cut -d' ' -f2 | tail -n 1)
	echo -n " in release: ${VERSION_IN_REL}"
	if [ "${RELEASE}" != "sid" ] ; then
		VERSION_IN_SEC=$(cat ${PKG_LIST_SEC} | grep '^'${SRC_PKG}' ' | cut -d' ' -f2 | tail -n 1)
		if [ -n "${VERSION_IN_SEC}" ] ; then
			echo -n " in security: ${VERSION_IN_SEC}"
		fi
	fi
	echo ")"
	if dpkg --compare-versions ${PKG_VER} lt ${VERSION_IN_REL} ; then
		echo "=========> NEEDS UPDATE (point release?) <========="
		rm -f SecuritySources Sources SecurityRelease Release SecurityInRelease InRelease
		exit 1
	fi
	if [ "${RELEASE}" != "sid" ] ; then
		if [ -n "${VERSION_IN_SEC}" ] ; then
			if dpkg --compare-versions ${PKG_VER} lt ${VERSION_IN_SEC} ; then
				echo "=========> NEED UPDATE (security) <========="
				rm -f SecuritySources Sources SecurityRelease Release SecurityInRelease InRelease
				exit 1
			fi
		fi
	fi
done
rm -f ${PKG_LIST_REL} ${PKG_LIST_SEC}

rm -f SecuritySources Sources SecurityRelease Release SecurityInRelease InRelease Release.gpg SecurityRelease.gpg

echo "=========> Check finished: ${RELEASE} does not need updating <========="

exit 0
